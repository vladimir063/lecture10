package org.sber.sber;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public interface MyStream<T> {

    MyStream<T> filter(Predicate<? super T> predicate);

    <R> MyStream<R> map(Function<? super T, ? extends R> mapper);

    MyStream<T> limit(long maxSize);

    MyStream<T> distinct();

    List<T> toList();

     <Q, K, U> Map<K,U> toMap(Function<? super Q, ? extends K> keyMapper, Function<? super Q, ? extends U> valueMapper) ;

    Optional<T> findFirst();

    void forEach(Consumer<? super T> action);

    long count();


}
