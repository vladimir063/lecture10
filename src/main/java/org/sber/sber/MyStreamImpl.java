package org.sber.sber;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyStreamImpl<T> implements MyStream<T> {

    private List<T> list;
    private Map<String, Object> map = new LinkedHashMap<>();

    private MyStreamImpl(List<T> list) {
        this.list = list;
    }

    public static <T> MyStream<T> of(List<T> list) {
        return new MyStreamImpl<>(list);
    }

    @Override
    public MyStream<T> filter(Predicate<? super T> predicate) {
        map.put("filter", predicate);
        return this;
    }

    private MyStream<T> filter2(Predicate<? super T> predicate) {
        for (int i = 0; i < list.size(); i++) {
            if (!predicate.test(list.get(i))) {
                list.remove(i);
                i--;
            }
        }
        return this;
    }

    @Override
    public <R> MyStream<R> map(Function<? super T, ? extends R> mapper) {
        map.put("map", mapper);
        return (MyStream<R>) this;
    }


    private <R> MyStream<R> map2(Object mapper) {
        Function<? super T, ? extends R> mapperNew = (Function<? super T, ? extends R>) mapper;
        List<R> listResult = new ArrayList<>();
        MyStream<R> newMyStream = new MyStreamImpl<>(listResult);
        for (T t : list) {
              listResult.add(mapperNew.apply(t));
        }
        list = (List<T>) listResult;
        return newMyStream;
    }

    @Override
    public MyStream<T> limit(long maxSize) {
        map.put("limit", maxSize);
        return this;
    }

    private MyStream<T> limit2(long maxSize) {
        list = list.subList(0, (int) maxSize);
        return this;
    }

    @Override
    public MyStream<T> distinct() {
        map.put("distinct", null);
        return this;
    }


    private MyStream<T> distinct2() {
        Set<T> set = new HashSet<>(list);
        List<T> newList = new ArrayList<>(set);
        list = newList;
        return this;
    }

    private void executeMethods(){
        for (Map.Entry<String,Object> entry : map.entrySet()){
            switch (entry.getKey()){
                case  "filter" : filter2((Predicate<? super T>) entry.getValue()); break;
                case "map" : map2((entry.getValue()));  break;
                case "limit" : limit2((Long) entry.getValue()); break;
                case "distinct" : distinct2(); break;
                default: break;
            }
        }
    }

    @Override
    public List<T> toList() {
        executeMethods();
        return list;
    }

    @Override
    public Optional<T> findFirst() {
        executeMethods();
        return Optional.of(list.get(0));
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        executeMethods();
        for (T t : list) {
            action.accept(t);
        }
    }

    @Override
    public long count() {
        executeMethods();
        return list.size();
    }

    @Override
    public <Q, K, U> Map<K, U> toMap(Function<? super Q, ? extends K> keyMapper, Function<? super Q, ? extends U> valueMapper) {
        executeMethods();
        Map<K, U> map = new HashMap<>();
        for (T t : list) {
            map.put(keyMapper.apply((Q) t), valueMapper.apply((Q) t));
        }
        return map;
    }
}

