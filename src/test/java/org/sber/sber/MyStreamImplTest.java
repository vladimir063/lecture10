package org.sber.sber;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class MyStreamImplTest<T> {

    MyStream<Integer> myStream;

    @BeforeEach
    void setUp() {
        List<Integer> list = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10 , 10));
        myStream = MyStreamImpl.of(list);
    }

    @Test
    void filter() {
        Optional<Integer> result = myStream.filter(x -> x == 5).findFirst();
        assertThat(result).isEqualTo(Optional.of(5));
    }

    @Test
    void map() {
        Optional<Integer> result = myStream.map(x -> x * 100).findFirst();
        assertThat(result).isEqualTo(Optional.of(100));
    }

    @Test
    void limitAndList() {
        List<Integer> list = myStream.limit(3).toList();
        assertThat(list).isEqualTo(List.of(1, 2 , 3));

    }

    @Test
    void distinct() {
        List<Integer> list = myStream.distinct().toList();
        assertThat(list.size()).isEqualTo(10);
    }


    @Test
    void count() {
        long count = myStream.count();
        assertThat(count).isEqualTo(13);
    }
}